var fs = require('fs'),
  DataCollection = require('data-collection');

module.exports = {
  uses: ['view', 'user_config'],
  init: function(view, user_config, options) {
    var self = this;
    this.view_data = new DataCollection([{
      title: 'DuckTales',
      thumbnail: user_config.get('user_data_dir') + '/thumbnails/Duck Tales.jpg', // @TODO user data dir from config
      year: '1990',
      description: 'DuckTales (わんぱくダック夢冒険 Wanpaku Dakku Yume Bōken?, lit. "Naughty Ducks Dream Adventures") is an action platformer video game developed by Capcom and based on the Disney animated TV series of the same name. It was first released in North America for the Nintendo Entertainment System in 1989 and was later ported to the Game Boy in 1990. The story involves Scrooge McDuck traveling across the globe collecting treasure and outwitting his rival Flintheart Glomgold to become the world\'s richest duck.'
    }, {
      title: 'Super Mario Bros. 3',
      thumbnail: user_config.get('user_data_dir') + '/thumbnails/smb3.jpg',
      year: '1990',
      description: 'Super Mario Bros. 3 centers on plumbers Mario and Luigi who embark on a quest to save Princess Toadstool and the rulers of seven different kingdoms from the antagonist Bowser and his children, the Koopalings. The player, as Mario or Luigi, is able to defeat enemies by stomping them or using items that bestow magical powers. Mario and Luigi are given a wider range of abilities than in previous Super Mario games, including flying or sliding down slopes. In addition, Super Mario Bros. 3 introduces numerous elements, such as new enemy characters and the use of a world map to transition between levels, that have reappeared in or have influenced subsequent Mario games.'
    }, {
      title: 'ジョイメカファイト',
      thumbnail: user_config.get('user_data_dir') + '/thumbnails/jmf.png',
      year: '1993',
      description: 'Joy Mech Fight (ジョイメカファイト Joi Meka Faito?), sometimes called Joy Mecha Fight, is a fighting game for the Family Computer, released only in Japan on May 21, 1993. The game was released during the generation shift between the Famicom and the newer Super Famicom, and Joy Mech Fight is counted among one of the most important late Famicom games for utilizing the console\'s audio and visual capabilities to the fullest extent'
    }]);

    this.view_data
      .query()
      .update({ selected: false })
      .first().selected = true;

    view('index', {
      games: this.view_data.query().values()
    }, self.element);

    // @HACK
//    fs.readdir('/home/james/media/Games/NES', function(err, files) {
//      var interval = setInterval(function(){
//        view_data.games.push(files.shift());
//        if (files.length === 0) {
//          clearInterval(interval);
//        }
//      }, 500);
//    });
  },
  events: {
    'a.back click': function(el, ev) {
      this.element.trigger('activity.back');
    },
    '.list-group a click': function(el, ev) {
      this.element.trigger('item.select', el.model());
    },
    'input.down': function(el, ev) {
      var $current = this.element.find('a.active'),
        $next = $current.next().length ?
          $current.next() : $current.siblings().first();

      $next.trigger('click');
    },
    'input.up': function(el, ev) {
      var $current = this.element.find('a.active'),
        $next = $current.prev().length ? $current.prev() :
          $current.siblings().last();

      $next.trigger('click');
    },
    'input.right': function(el, ev) {
      var $current = this.element.find('a.active'),
        $all = $current.siblings().addBack(),
        active = $all.eq($current.index() + 10);

      if (!active.length) {
        active = $all.last();
      }

      this.element.trigger('item.select', active.model());
    },
    'input.left': function(el, ev) {
      var $current = this.element.find('a.active'),
        $all = $current.siblings().addBack(),
        active = $all.eq($current.index() - 10);

      if (!active.length) {
        active = $all.first();
      }

      this.element.trigger('item.select', active.model());
    },
    'item.select': function(el, ev, game) {
      this.view_data
        .query()
        .exclude({ title: game.title })
        .update({
          selected: false
        });

      game.selected = true;
    }
  },
  intents: {
    filters: ['ACTIVITY_GAMES']
  }
};
